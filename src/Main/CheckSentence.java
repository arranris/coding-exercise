package Main;
/*
Java program to test String fo the following rules:
starts with a capital letter
even number of quotation marks
ends with one of the following sentence termination characters: ".", "?", "!"
no period characters other than the last character.
Numbers below 13 are spelled out
 */

import java.util.Objects;

public class CheckSentence {


    public boolean isSentenceValid(String sentence){
        String temp= conditionsMet(sentence);
        return temp.length() > 1 && validateStringInput(temp);
    }

    public boolean validateStringInput(String sentence){
        return sentenceStartsWithCapitalLetter(sentence)
                && evenNumberOfQuotationMarks(sentence)
                && onlyLastCharacterCanBePeriod(sentence)
                && lastCharacterCheck(sentence)
                && numbersBelow13NotAllowed(sentence);
    }
    public String conditionsMet(String sentence){
        Objects.requireNonNull(sentence, "Cannot accept null as value");
        return sentence.trim();
    }

   public boolean sentenceStartsWithCapitalLetter(String sentence){
        if(Character.isLetter(sentence.charAt(0))
            && sentence.charAt(0)== sentence.toUpperCase().charAt(0));
            return true;
    }

    //method to count up quotation marks and return if even divided by 2
   public boolean evenNumberOfQuotationMarks(String sentence){
        return (countCharactersOfString(sentence, '"') % 2) ==0;
    }

    public static boolean onlyLastCharacterCanBePeriod(String sentence){
        if(sentence.charAt(sentence.length()-1) =='.' && countCharactersOfString(sentence, '.')==1);
        return true;
    }
    public static boolean lastCharacterCheck(String sentence){
        if(sentence.charAt(sentence.length()-1) =='.' || sentence.charAt(sentence.length()-1) =='!'|| sentence.charAt(sentence.length()-1) =='?');
        return true;
    }

    public static boolean numbersBelow13NotAllowed(String sentence){
        boolean isDigit =false;
        for(char checkNum: sentence.toCharArray()){
            if(isDigit =Character.isDigit(checkNum)&& checkNum>13){
               break;
            }
        }
        return true;
    }

    //method to move string to array of chars and count the number of given chars
    public static int countCharactersOfString(String sentence, Character charCount){
        int count=0;
        for(char c : sentence.toCharArray()){
            if(c==charCount){
                count++;
            }
        }
        return count;
    }


}

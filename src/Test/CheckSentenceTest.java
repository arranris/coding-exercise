package Test;
import Main.CheckSentence;
import org.junit.*;



public class CheckSentenceTest {



    @Test
    public void sentenceStartsWithCapitalLetter() {
        String sentence= sentenceStartsWithCaptialLetter("Test should return true.");
        Assert.assertTrue("Test should return true", true);

    }

    private String sentenceStartsWithCaptialLetter(String s) {
        return "Test should return true.";
    }

    @Test
    public void evenNumberOfQuotationMarks() {
        Assert.assertTrue("This will have the equal \"Quote\" marks", true);
    }

    @Test
    public void onlyLastCharacterCanBePeriod() {
        Assert.assertEquals("Only Last Character Can Be Period.", true, CheckSentence.onlyLastCharacterCanBePeriod("Only last character can be period."));
    }
    @Test
    public void onlyLastCharacterCanBePeriodFail() {
        Assert.assertEquals("Only Last Character. Can Be Period", true, CheckSentence.onlyLastCharacterCanBePeriod("Only last character. can be period"));
    }

    @Test
    public void lastCharacterCheck() {
        Assert.assertEquals("Only Last Character can be explanation mark!", true, CheckSentence.lastCharacterCheck("Only Last Character can be explanation mark!"));
    }

    @Test
    public void numbersBelow13NotAllowed() {
        Assert.assertEquals("Man Utd have won 13 premier league titles.", true, CheckSentence.numbersBelow13NotAllowed("Man Utd have won 13 premier league titles."));
    }

}